package com.davistech.lightbulb

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.RadioButton
import androidx.navigation.fragment.NavHostFragment
import com.davistech.lightbulb.databinding.ActivityMainBinding
import com.google.android.material.navigation.NavigationBarView
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Apply animation
        val live: Animation = AnimationUtils.loadAnimation(this, R.anim.trans_scale)
        binding.card.startAnimation(live)


        //Check blue radio
        //binding.radioGroup.check((if (getPref(color) is String ) getPref(color) else R.id.radioBlue) as Int)


        binding.nameBulb.setOnFocusChangeListener{nameBulb, hasFocus ->
            if (nameBulb is EditText) {
                if (!hasFocus){
                    writePref(
                        name,
                        nameBulb.text.toString()
                    )
                }
            }
        }
        // To listen for a switch's checked/unchecked state changes
        binding.switchOnOff.setOnCheckedChangeListener { _, isChecked ->
            toggleBulb(isChecked)
        }
    }

    //Handle Radio Button Clicks
    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                R.id.radioRed ->
                    if (checked) {
                        writePref(color, "red")
                        setImageRes()
                    }
                R.id.radioGreen ->
                    if (checked) {
                        writePref(color, "green")
                        setImageRes()
                    }
                R.id.radioBlue ->
                    if (checked) {
                        writePref(color, "blue")
                        setImageRes()
                    }
            }
        }
    }

    fun onSaveButtonClicked(view: View) {
        val value = binding.nameBulb.text.toString()
        writePref(name, value)
        getPref(name)?.let { Snackbar.make(binding.coordinator, "Name saved as $it", 2000 ).show() }
    }

    private fun setImageRes(isChecked: Boolean = binding.switchOnOff.isChecked) {
        var res = if (!isChecked) R.drawable.ic_bulb_off else adjustColor()
        binding.bulb.setImageResource(res)
    }


    private fun toggleBulb(isChecked: Boolean){
        setImageRes(isChecked)
    }

    private fun adjustColor() : Int{
        when(getPref(color)){
            "red" -> return R.drawable.ic_bulb_red
            "green" -> return  R.drawable.ic_bulb_green
            "blue" -> return R.drawable.ic_bulb_blue
        }
        return R.drawable.ic_bulb_off
    }

    private fun writePref(key:String, value: String){
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString(key, value)
            apply()
        }
    }

    private fun getPref(key: String, default: String = "blue"): String? {
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        if (sharedPref is SharedPreferences) {
            return sharedPref.getString(key,null)
        }
        return "off"
    }

    companion object{
        private const val name = "com.davistech.lightbulb.name"
        private const val color = "com.davistech.lightbulb.color"
    }
}